package com.ruoyi.setting.vo;

/**
 * Created by 魔金商城 on 17/11/21.
 * 区域枚举
 */
public enum AreaItem {
    CITY, DISTRICT, NO, ALL
}
